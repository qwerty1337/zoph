<?php
/*
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

use conf\conf;
use template\template;

function create_field_html($fields) {

    $html = "";
    foreach ($fields as $key => $val) {
        if ($val) {
            $html .=
            "<dt>" . e($key) . "</dt>\n" .
            "<dd>" . $val ." </dd>\n";
        }
    }
    return $html;
}

function create_edit_fields($fields) {
    $html = "";
    foreach ($fields as $key => $val) {
        $html.=
            "<label for=\"$key\">$val[0]</label>\n" . $val[1] ."<br>";
    }
    return $html;
}

function create_text_input($name, $value, $size = 20, $max = 32, $type="text") {
    if ($type=="time") {
        $step="step=\"1\"";
    } else {
        $step="";
    }
    $id=preg_replace("/^_+/", "", $name);
    return "<input type=\"$type\" $step name=\"$name\" id=\"$id\" value=\"" . e($value) ."\"
        size=\"$size\" maxlength=\"$max\">\n";
}

function create_integer_pulldown($name, $value, $min, $max) {
    for ($i = $min; $i <= $max; $i++) {
        $integer_array["$i"] = $i;
    }
    return template::createPulldown($name, $value, $integer_array);
}

/*
 * Updates a query string, replacing (or inserting) a key.
 * A list of keys to ignore can also be specified.
 */
function update_query_string($vars, $new_key, $new_val, $ignore = null) {

    if (!$ignore) { $ignore = array(); }
    $ignore[] = "PHPSESSID";
    $ignore[] = "_crumb";
    $qstr="";
    if ($vars) {
        foreach ($vars as $key => $val) {
            if (in_array($key, $ignore)) { continue; }
            if ($key == $new_key) { continue; }

            if (!empty($qstr)) { $qstr .= "&amp;"; }
            if (is_array($val)) {
                $qstr .= rawurlencode_array($key, $val);
            } else {
                $qstr .= rawurlencode($key) . "=" . rawurlencode($val);
            }
        }
    }

    if ($new_key && isset($new_val)) {
        if ($qstr) { $qstr .= "&amp;"; }
        if (is_array($new_val)) {
            $qstr .= rawurlencode_array($new_key, $new_val);
        } else {
            $qstr .= rawurlencode($new_key) . "=" . rawurlencode($new_val);
        }
    }

    return $qstr;
}

function create_form($vars, $ignore = array()) {

    $ignore[] = "PHPSESSID";
    $ignore[] = "_crumb";

    $form = "";
    foreach ($vars as $key => $val) {
        if (in_array($key, $ignore)) { continue; }
        $form .= "<input type=\"hidden\" name=\"$key\" value=\"" . e($val) . "\">\n";
    }

    return $form;
}

/**
 * Create a link to the calendar page
 * @param string Date in "yyyy-mm-dd" format
 * @param string Search field, the field to search from from the calendar page
 * @return string link.
 * @todo Contains HTML
 * @todo Should be better separated, possibly included in Time object
 */
function create_date_link($date, $search_field = "date") {
    $dt = new Time($date);

    if ($date) {
        $html="<a href=\"calendar.php?date=$date&amp;search_field=$search_field\">";
        $html.=$dt->format(conf::get("date.format"));
        $html.="</a>";
        return $html;
    }
}

/*
 * Encode URL raw
 * based on urlencode_array
 * By linus at flowingcreativity dot net
 * from: http://www.php.net/manual/en/function.urlencode.php
 * @param array the array value
 * @param string variable name to be used in the query string
 * @param string what separating character to use in the query string
 */
function rawurlencode_array($var, $varName, $separator = '&') {
    $toImplode = array();
    foreach ($var as $key => $value) {
        if (is_array($value)) {
            $toImplode[] = rawurlencode_array($value, "{$varName}[{$key}]", $separator);
        } else {
            $toImplode[] = "{$varName}[{$key}]=".rawurlencode($value);
        }
    }
    return implode($separator, $toImplode);
}

function redirect($url = "zoph.php", $msg = "Access denied") {
    if (!((LOG_SUBJECT & log::REDIRECT) && (LOG_SEVERITY >= log::DEBUG))) {
        header("Location: " . $url);
    }
        echo "<a href='" . $url . "'>" . $msg . "</a>";
    die();
}


?>
