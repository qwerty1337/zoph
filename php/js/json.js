
// This file is part of Zoph.
//
// Zoph is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// Zoph is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// You should have received a copy of the GNU General Public License
// along with Zoph; if not, write to the Free Software
// Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA


var zJSON=function() {
    var retry;

    function getData(object, search) {
        var http=new XMLHttpRequest();

        if(object=='locationLookup') {
            // currently the only supported
        } else {
            return;
        }

        var url="service/" + object + ".php?search=" + escape(search);

        if (http) {
            input=document.getElementById('_' . object);
            http.open("GET", url, true);
            http.onreadystatechange=function() {
               httpResponse(http, object);
            };
            http.send(null);
        } else {
            // try again in 500 ms
            clearTimeout(retry);
            retry=setTimeout("JSON.getData('" + object + "','" + search + "')", 500);
        }
    }

    function httpResponse(http, object) {
        input=document.getElementById(object);
        if (http.readyState == 4) {
            if(http.status == 200) {
                locationLookup.httpResponse(object, http.response);
            }
        }
    }

    return {
        getData:getData,
        httpResponse:httpResponse
    };
}();
